/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define MUX_S0_Pin GPIO_PIN_2
#define MUX_S0_GPIO_Port GPIOE
#define MUX_S1_Pin GPIO_PIN_3
#define MUX_S1_GPIO_Port GPIOE
#define MUX_S2_Pin GPIO_PIN_4
#define MUX_S2_GPIO_Port GPIOE
#define MUX_S3_Pin GPIO_PIN_5
#define MUX_S3_GPIO_Port GPIOE
#define SW_I2C_SCL_Pin GPIO_PIN_0
#define SW_I2C_SCL_GPIO_Port GPIOC
#define SW_I2C_SDA_Pin GPIO_PIN_1
#define SW_I2C_SDA_GPIO_Port GPIOC
#define PWR_EN0_Pin GPIO_PIN_7
#define PWR_EN0_GPIO_Port GPIOE
#define PWR_EN1_Pin GPIO_PIN_8
#define PWR_EN1_GPIO_Port GPIOE
#define PWR_EN2_Pin GPIO_PIN_9
#define PWR_EN2_GPIO_Port GPIOE
#define PWR_EN3_Pin GPIO_PIN_10
#define PWR_EN3_GPIO_Port GPIOE
#define PWR_EN4_Pin GPIO_PIN_11
#define PWR_EN4_GPIO_Port GPIOE
#define PWR_EN5_Pin GPIO_PIN_12
#define PWR_EN5_GPIO_Port GPIOE
#define PWR_EN6_Pin GPIO_PIN_13
#define PWR_EN6_GPIO_Port GPIOE
#define PWR_EN7_Pin GPIO_PIN_14
#define PWR_EN7_GPIO_Port GPIOE
#define PWR_EN8_Pin GPIO_PIN_15
#define PWR_EN8_GPIO_Port GPIOE
#define PWR_EN9_Pin GPIO_PIN_10
#define PWR_EN9_GPIO_Port GPIOB
#define MUX_EN19_Pin GPIO_PIN_11
#define MUX_EN19_GPIO_Port GPIOB
#define MUX_EN18_Pin GPIO_PIN_12
#define MUX_EN18_GPIO_Port GPIOB
#define MUX_EN17_Pin GPIO_PIN_13
#define MUX_EN17_GPIO_Port GPIOB
#define MUX_EN16_Pin GPIO_PIN_14
#define MUX_EN16_GPIO_Port GPIOB
#define MUX_EN15_Pin GPIO_PIN_15
#define MUX_EN15_GPIO_Port GPIOB
#define MUX_EN14_Pin GPIO_PIN_8
#define MUX_EN14_GPIO_Port GPIOD
#define MUX_EN13_Pin GPIO_PIN_9
#define MUX_EN13_GPIO_Port GPIOD
#define MUX_EN12_Pin GPIO_PIN_10
#define MUX_EN12_GPIO_Port GPIOD
#define MUX_EN11_Pin GPIO_PIN_11
#define MUX_EN11_GPIO_Port GPIOD
#define MUX_EN10_Pin GPIO_PIN_12
#define MUX_EN10_GPIO_Port GPIOD
#define MUX_EN9_Pin GPIO_PIN_13
#define MUX_EN9_GPIO_Port GPIOD
#define MUX_EN8_Pin GPIO_PIN_14
#define MUX_EN8_GPIO_Port GPIOD
#define MUX_EN7_Pin GPIO_PIN_15
#define MUX_EN7_GPIO_Port GPIOD
#define MUX_EN6_Pin GPIO_PIN_6
#define MUX_EN6_GPIO_Port GPIOC
#define MUX_EN5_Pin GPIO_PIN_7
#define MUX_EN5_GPIO_Port GPIOC
#define MUX_EN4_Pin GPIO_PIN_8
#define MUX_EN4_GPIO_Port GPIOC
#define MUX_EN3_Pin GPIO_PIN_9
#define MUX_EN3_GPIO_Port GPIOC
#define MUX_EN2_Pin GPIO_PIN_8
#define MUX_EN2_GPIO_Port GPIOA
#define MUX_EN1_Pin GPIO_PIN_0
#define MUX_EN1_GPIO_Port GPIOD
#define MUX_EN0_Pin GPIO_PIN_1
#define MUX_EN0_GPIO_Port GPIOD

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

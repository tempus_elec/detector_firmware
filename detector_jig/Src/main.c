
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */
#include "bsp/bsp.h"
#include "bsp/bsp_tempus.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

UART_HandleTypeDef huart4;
UART_HandleTypeDef huart1;

osThreadId defaultTaskHandle;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint8_t message[100];
uint8_t msg_buf[MAX_PACKET_LENGTH];
uint8_t send_msg[MAX_PACKET_LENGTH];
uint8_t ch;
uint16_t adc_val[ADC_COUNT];
extern uint16_t adc_buf[ADC_COUNT];
uint32_t adc_read_val[ADC_COUNT];
uint8_t test_msg[100];
uint32_t delay_us_delay;


uint8_t msg_length = 0;


extern TEMPUS_ST g_Tempus;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_UART4_Init(void);
static void MX_USART1_UART_Init(void);
void StartDefaultTask(void const * argument);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_UART4_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
 

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV4;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 10;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_41CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_2;
  sConfig.Rank = ADC_REGULAR_RANK_3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = ADC_REGULAR_RANK_4;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = ADC_REGULAR_RANK_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_5;
  sConfig.Rank = ADC_REGULAR_RANK_6;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = ADC_REGULAR_RANK_7;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_7;
  sConfig.Rank = ADC_REGULAR_RANK_8;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_14;
  sConfig.Rank = ADC_REGULAR_RANK_9;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_15;
  sConfig.Rank = ADC_REGULAR_RANK_10;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* UART4 init function */
static void MX_UART4_Init(void)
{

  huart4.Instance = UART4;
  huart4.Init.BaudRate = 115200;
  huart4.Init.WordLength = UART_WORDLENGTH_8B;
  huart4.Init.StopBits = UART_STOPBITS_1;
  huart4.Init.Parity = UART_PARITY_NONE;
  huart4.Init.Mode = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart4.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, MUX_S0_Pin|MUX_S1_Pin|MUX_S2_Pin|MUX_S3_Pin 
                          |PWR_EN0_Pin|PWR_EN1_Pin|PWR_EN2_Pin|PWR_EN3_Pin 
                          |PWR_EN4_Pin|PWR_EN5_Pin|PWR_EN6_Pin|PWR_EN7_Pin 
                          |PWR_EN8_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, SW_I2C_SCL_Pin|SW_I2C_SDA_Pin|MUX_EN6_Pin|MUX_EN5_Pin 
                          |MUX_EN4_Pin|MUX_EN3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, PWR_EN9_Pin|MUX_EN19_Pin|MUX_EN18_Pin|MUX_EN17_Pin 
                          |MUX_EN16_Pin|MUX_EN15_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, MUX_EN14_Pin|MUX_EN13_Pin|MUX_EN12_Pin|MUX_EN11_Pin 
                          |MUX_EN10_Pin|MUX_EN9_Pin|MUX_EN8_Pin|MUX_EN7_Pin 
                          |MUX_EN1_Pin|MUX_EN0_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(MUX_EN2_GPIO_Port, MUX_EN2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : MUX_S0_Pin MUX_S1_Pin MUX_S2_Pin MUX_S3_Pin 
                           PWR_EN0_Pin PWR_EN1_Pin PWR_EN2_Pin PWR_EN3_Pin 
                           PWR_EN4_Pin PWR_EN5_Pin PWR_EN6_Pin PWR_EN7_Pin 
                           PWR_EN8_Pin */
  GPIO_InitStruct.Pin = MUX_S0_Pin|MUX_S1_Pin|MUX_S2_Pin|MUX_S3_Pin 
                          |PWR_EN0_Pin|PWR_EN1_Pin|PWR_EN2_Pin|PWR_EN3_Pin 
                          |PWR_EN4_Pin|PWR_EN5_Pin|PWR_EN6_Pin|PWR_EN7_Pin 
                          |PWR_EN8_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : SW_I2C_SCL_Pin SW_I2C_SDA_Pin MUX_EN6_Pin MUX_EN5_Pin 
                           MUX_EN4_Pin MUX_EN3_Pin */
  GPIO_InitStruct.Pin = SW_I2C_SCL_Pin|SW_I2C_SDA_Pin|MUX_EN6_Pin|MUX_EN5_Pin 
                          |MUX_EN4_Pin|MUX_EN3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PWR_EN9_Pin MUX_EN19_Pin MUX_EN18_Pin MUX_EN17_Pin 
                           MUX_EN16_Pin MUX_EN15_Pin */
  GPIO_InitStruct.Pin = PWR_EN9_Pin|MUX_EN19_Pin|MUX_EN18_Pin|MUX_EN17_Pin 
                          |MUX_EN16_Pin|MUX_EN15_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : MUX_EN14_Pin MUX_EN13_Pin MUX_EN12_Pin MUX_EN11_Pin 
                           MUX_EN10_Pin MUX_EN9_Pin MUX_EN8_Pin MUX_EN7_Pin 
                           MUX_EN1_Pin MUX_EN0_Pin */
  GPIO_InitStruct.Pin = MUX_EN14_Pin|MUX_EN13_Pin|MUX_EN12_Pin|MUX_EN11_Pin 
                          |MUX_EN10_Pin|MUX_EN9_Pin|MUX_EN8_Pin|MUX_EN7_Pin 
                          |MUX_EN1_Pin|MUX_EN0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : MUX_EN2_Pin */
  GPIO_InitStruct.Pin = MUX_EN2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(MUX_EN2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	if(hadc == &hadc1){
		for(int i=0;i<ADC_COUNT;i++)
		{
			adc_buf[i] = adc_val[i];
		}
	}
}
/* USER CODE END 4 */

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{

  /* USER CODE BEGIN 5 */
	bsp_tempus_init();
	osDelay(5);
	bsp_mux_en_deinit();
	osDelay(5);
	bsp_ldo_deinit();
	osDelay(5);
	bsp_mux_en_init();
	osDelay(5);
	bsp_ldo_init();


	HAL_StatusTypeDef isRev;
	uint8_t rev_length = 0;
	HAL_ADC_Start_DMA(&hadc1, (uint16_t*)adc_val,ADC_COUNT);
	HAL_ADC_Start_IT(&hadc1);
  /* Infinite loop */
  for(;;)
  {

	  /*

	  bsp_set_mux_for_sensor(89 , 0);
	  osDelay(2);
	  bsp_read_no_filter();
	  osDelay(50);
	  */






	  isRev = HAL_UART_Receive(&huart4 , msg_buf , 1 , 3000);
	  	  rev_length ++;

	  	  while(isRev == HAL_OK && rev_length < MSG_LENGTH){
	  	  	 isRev = HAL_UART_Receive(&huart4 , msg_buf + rev_length, 1 , 50);
	  	  	 rev_length ++;
	  	  }

	  	  if(rev_length != MSG_LENGTH)
	  	  {
	  	  	 rev_length = 0;
	  	  	 continue;
	  	  }


	  	  rev_length = 0x0;



	  	  uint16_t rev_crc = ToUShort(msg_buf[CRC_START_INDEX + 1] , msg_buf[CRC_START_INDEX]);
	  	  uint16_t get_crc = get_crc16(msg_buf , CRC_START_INDEX);

	  	  if(ToUShort(msg_buf[0] , msg_buf[1]) != 0xaa55)
	  	  	 continue;
	  	  if(get_crc != rev_crc){
	  	  	 continue;
	  	  }

	  	  uint8_t cmd = msg_buf[2];
	  	  uint8_t cur_index;

	  	  if(cmd == zmdi_setting)
	  	  {
	  	  	 uint8_t index = msg_buf[3];
	  	  	 uint8_t sub_index = msg_buf[4];
	  	  	 uint8_t i2c_addr = msg_buf[5];
	  	  	 uint16_t zmdi_id;

	  	  	 if(index > 99 || sub_index > 1)
	  	  	  	continue;

	  	  	 bsp_set_mux_for_sensor(index , sub_index);
	  	  	 osDelay(10);
	  	  	 bsp_tempus_i2c_addr_set(i2c_addr );
	  	  	 init_Setting();
	  	  	 osDelay(1);
	  	  	 zmdi_id = read_id();
	  	  	 osDelay(5);


	  	  	 cur_index = DATA_START_INDEX;
	  	  	 send_msg[cur_index++] = index;
	  	  	 send_msg[cur_index++] = sub_index;
	  	  	 send_msg[cur_index++] = i2c_addr;
	  	  	 send_msg[cur_index++] = ToByte(zmdi_id , 0);
	  	  	 send_msg[cur_index++] = ToByte(zmdi_id , 1 );
	  	  	 send_msg[cur_index++] = g_Tempus.status;


	  	  	 copy_adc_value(&send_msg[cur_index]);
	  	  	 cur_index += ADC_BUF_SIZE;

	  	  	 for(int i = cur_index; i < CRC_START_INDEX; i++)
	  	  	  	send_msg[cur_index++] = 0x0;

	  	  } else if(cmd == read_Sensor_filter){

	  	  	 uint8_t index = msg_buf[3];
	  	  	 uint8_t sub_index = msg_buf[4];
	  	  	 uint8_t i2c_addr = msg_buf[5];
	  	  	 uint16_t zmdi_id;
	  	  	 uint8_t loop_count = 0x0;


	  	  	 if(index > 99 || sub_index > 1)
	  	  	  	  continue;

	  	  	 bsp_set_mux_for_sensor(index , sub_index);
	  	  	 osDelay(10);
	  	  	 bsp_tempus_i2c_addr_set(i2c_addr );
	  	  	 bsp_read_filter();
	  	  	 osDelay(1);
	  	  	 zmdi_id = read_id();
	  	  	 osDelay(5);
	  	  	 cur_index = DATA_START_INDEX;

	  	  	 send_msg[cur_index++] = index;
	  	  	 send_msg[cur_index++] = sub_index;
	  	  	 send_msg[cur_index++] = i2c_addr;
	  	  	 send_msg[cur_index++] = ToByte(zmdi_id , 0);
	  	  	 send_msg[cur_index++] = ToByte(zmdi_id , 1 );
	  	  	 send_msg[cur_index++] = g_Tempus.status;
	  	  	 send_msg[cur_index++] = g_Tempus.sensor_low;
	  	  	 send_msg[cur_index++] = g_Tempus.sensor_mid;
	  	  	 send_msg[cur_index++] = g_Tempus.sensor_high;
	  	  	 send_msg[cur_index++] = 0x0;
	  	  	 send_msg[cur_index++] = g_Tempus.temp_low;
	  	  	 send_msg[cur_index++] = g_Tempus.temp_mid;
	  	  	 send_msg[cur_index++] = g_Tempus.temp_high;
	  	  	 send_msg[cur_index++] = 0x0;



	  	  	 for(int i=cur_index;i<CRC_START_INDEX;i++)
	  	  	   send_msg[cur_index++] = 0x0;




	  	  } else if(cmd == read_Sensor_No_filter){

	  	  	 uint8_t index = msg_buf[3];
	  	  	 uint8_t sub_index = msg_buf[4];
	  	  	 uint8_t i2c_addr = msg_buf[5];
	  	  	 uint16_t zmdi_id;

	  	  	 if(index > 99 || sub_index > 1)
	  	  	  	 continue;


	  	  	 bsp_set_mux_for_sensor(index , sub_index);
	  	  	 osDelay(10);
	  	  	 bsp_tempus_i2c_addr_set(i2c_addr );


	  	  	 zmdi_id = read_id();
	  	  	 osDelay(1);
	  	  	 bsp_read_no_filter();
	  	  	 osDelay(5);

	  	  	 uint8_t ret_status = g_Tempus.status;


	  	  	 cur_index = DATA_START_INDEX;

	  	  	 send_msg[cur_index++] = index;
	  	  	 send_msg[cur_index++] = sub_index;
	  	  	 send_msg[cur_index++] = i2c_addr;
	  	  	 send_msg[cur_index++] = ToByte(zmdi_id , 0);
	  	  	 send_msg[cur_index++] = ToByte(zmdi_id , 1 );
	  	  	 send_msg[cur_index++] = ret_status;
	  	  	 send_msg[cur_index++] = g_Tempus.sensor_low;
	  	  	 send_msg[cur_index++] = g_Tempus.sensor_mid;
	  	  	 send_msg[cur_index++] = g_Tempus.sensor_high;
	  	  	 send_msg[cur_index++] = 0x0;
	  	  	 send_msg[cur_index++] = g_Tempus.temp_low;
	  	  	 send_msg[cur_index++] = g_Tempus.temp_mid;
	  	  	 send_msg[cur_index++] = g_Tempus.temp_high;
	  	  	 send_msg[cur_index++] = 0x0;




	  	  	 for(int i=cur_index;i<CRC_START_INDEX;i++)
	  	  	 	send_msg[cur_index++] = 0x0;

	  	  } else if( cmd == zmdi_write_id ){

	  	  	 uint8_t index = msg_buf[3];
	  	  	 uint8_t sub_index = msg_buf[4];
	  	  	 uint8_t i2c_addr = msg_buf[5];
	  	  	 uint16_t send_id = 0x0;
	  	  	 uint8_t loop_count = 0x0;

	  	  	 if(index > 99 || sub_index > 1)
	  	  	     continue;

	  	  	 bsp_set_mux_for_sensor(index , sub_index);
	  	  	 osDelay(10);
	  	  	 bsp_tempus_i2c_addr_set(i2c_addr );
	  	  	 uint16_t s_id = ToUShort( msg_buf[6] , msg_buf[7] );
	  	  	 uint8_t ret_status = 0x0;

	  	  	 do{
	  	  	   	  write_id(s_id);
	  	  	  	  if( g_Tempus.status != I2C_OK){
	  	  	     		send_id = 0xffff;
	  	  	     		break;
	  	  	   	  }
	  	  	 	 send_id = read_id();
	  	  	 	 loop_count++;
	  	  	 }while((send_id != s_id) &&( loop_count < 15));

	  	  	 osDelay(5);

	  	  	 ret_status = g_Tempus.status;

	  	  	 cur_index = DATA_START_INDEX;
	  	  	 send_msg[cur_index++] = index;
	  	  	 send_msg[cur_index++] = sub_index;
	  	  	 send_msg[cur_index++] = i2c_addr;
	  	  	 send_msg[cur_index++] = ToByte(send_id , 0);
	  	  	 send_msg[cur_index++] = ToByte(send_id , 1);
	  	  	 send_msg[cur_index++] = g_Tempus.status;




	  	  	 for(int i=cur_index;i<CRC_START_INDEX;i++)
	  	  	  	 send_msg[cur_index++] = 0x0;

	  	  } else if( cmd == i2c_addr_write){

	  	  	 uint8_t index = msg_buf[3];
	  	  	 uint8_t sub_index = msg_buf[4];
	  	  	 uint8_t i2c_addr = msg_buf[5];
	  	  	 uint8_t write_i2c_addr = msg_buf[6];
	  	  	 uint16_t zmdi_id = 0x0;

	  	  	 if(index > 99 || sub_index > 1)
	  	  	  	 continue;
	  	  	 bsp_set_mux_for_sensor(index , sub_index);
	  	  	 osDelay(10);
	  	  	 bsp_tempus_i2c_addr_set(i2c_addr );
	  	  	 zmdi_id = read_id();
	  	  	 bsp_i2c_addr_write(write_i2c_addr);
	  	  	 osDelay(5);

	  	  	 cur_index = DATA_START_INDEX;
	  	  	 send_msg[cur_index++] = index;
	  	  	 send_msg[cur_index++] = sub_index;
	  	  	 send_msg[cur_index++] = i2c_addr;
	  	  	 send_msg[cur_index++] = ToByte(zmdi_id , 0);
	  	  	 send_msg[cur_index++] = ToByte(zmdi_id , 1);
	  	  	 send_msg[cur_index++] = g_Tempus.status;



	  	  	 for(int i=cur_index;i<CRC_START_INDEX;i++)
	  	  	  	send_msg[cur_index++] = 0x0;



	  	  } else if(cmd == i2c_scan){

	  	  	 uint8_t index = msg_buf[3];
	  	  	 uint8_t sub_index = msg_buf[4];
	  	  	 uint8_t ret_i2c_addr = msg_buf[5];

	  	  	 if(index > 99 || sub_index > 1)
	  	  	  	 continue;

	  	  	 bsp_set_mux_for_sensor(index , sub_index);
	  	  	 osDelay(10);
	  	  	 ret_i2c_addr = bsp_i2c_scan();
	  	  	 osDelay(5);

	  	  	 cur_index = DATA_START_INDEX;
	  	  	 send_msg[cur_index++] = index;
	  	  	 send_msg[cur_index++] = sub_index;
	  	  	 send_msg[cur_index++] = ret_i2c_addr;
	  	  	 send_msg[cur_index++] = 0x0;
	  	  	 send_msg[cur_index++] = 0x0;
	  	  	 send_msg[cur_index++] = g_Tempus.status;
	  	  	 send_msg[cur_index++] = ret_i2c_addr;




	  	  	 for(int i=cur_index;i<CRC_START_INDEX;i++)
	  	  	  	 send_msg[cur_index++] = 0x0;
	  	  	 osDelay(10);

	  	  } else
	  	  	 continue;


	  	  if( cmd != i2c_scan)
	  	              	 osDelay(80);

	  	  send_msg[0] = 0xbb;
	  	  send_msg[1] = 0x66;
	  	  send_msg[2] = cmd + 1;

	  	  uint16_t send_crc = get_crc16(send_msg , CRC_START_INDEX);
	  	  send_msg[CRC_START_INDEX] = ToByte(send_crc , 0);
	  	  send_msg[CRC_START_INDEX + 1] = ToByte(send_crc , 1);

	  	  	  	  	 //CDC_Transmit_FS(send_msg , 20);
	  	  HAL_UART_Transmit(&huart4 , send_msg , MSG_LENGTH ,1000);




  }
  /* USER CODE END 5 */ 
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/*
 * bsp_mux.h
 *
 *  Created on: 2018. 6. 18.
 *      Author: jhpark
 */

#ifndef BSP_BSP_MUX_H_
#define BSP_BSP_MUX_H_


#define MUX_COUNT 20
#define MUX_CHANNLE_COUNT  10

#define S0   0x1
#define S1   0x2
#define S2   0x4
#define S3   0x8


#endif /* BSP_BSP_MUX_H_ */

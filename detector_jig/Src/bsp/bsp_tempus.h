/*
 * bsp_tempus.h
 *
 *  Created on: 2018. 6. 18.
 *      Author: jhpark
 */

#ifndef BSP_BSP_TEMPUS_H_
#define BSP_BSP_TEMPUS_H_


#define I2C_ADDR_TEMPUS     0x00

#define TEMPUS_I2C_ADR_W    0x00
#define TEMPUS_I2C_ADR_R    0x01

#define TEMPUS_AVR_COUNT    1
#define TEMPUS_BUF_SIZE     12

/*
    TEMPUS CMD List
*/
#define CMD_MEASURE     0xAA
#define Start_NOM       0xA8
#define Start_CM        0xA9


#define Gain_T 4211026
#define SOT_T  83355
#define Offset_T  670000
#define Gain_B  1048576

typedef struct TEMPUS_S
{
	uint8_t status;
	uint8_t mem1;
	uint8_t mem2;

    uint8_t sensor_high;
    uint8_t sensor_mid;
    uint8_t sensor_low;
    uint8_t sensor_rsv;

    uint8_t temp_high;
    uint8_t temp_mid;
    uint8_t temp_low;
    uint8_t temp_rsv;

    int32_t rev_SOT_T;
    int32_t rev_SOT_B;
    int32_t rev_Gain_T;
    int32_t rev_Gain_B;
    int32_t rev_Offset_T;
    int32_t rev_Offset_B;
    int32_t rev_Sens_Shift;

    float sensor;
    float temperature;
    float t_obj;
}TEMPUS_ST;




#endif /* BSP_BSP_TEMPUS_H_ */

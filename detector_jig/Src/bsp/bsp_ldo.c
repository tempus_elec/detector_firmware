/*
 * bsp_ldo.c
 *
 *  Created on: 2018. 6. 18.
 *      Author: jhpark
 */



#include <stdint.h>
#include <stdbool.h>
#include "string.h"
#include "bsp.h"
#include "bsp_ldo.h"
#include "main.h"

inline GPIO_TypeDef * ldo_port(uint16_t x)
{
	switch(x)
	{
		case 0:
			return PWR_EN0_GPIO_Port;
			break;
		case 1:
			return PWR_EN1_GPIO_Port;
			break;
		case 2:
			return PWR_EN2_GPIO_Port;
			break;
		case 3:
			return PWR_EN3_GPIO_Port;
			break;
		case 4:
			return PWR_EN4_GPIO_Port;
			break;
		case 5:
			return PWR_EN5_GPIO_Port;
			break;
		case 6:
			return PWR_EN6_GPIO_Port;
			break;
		case 7:
			return PWR_EN7_GPIO_Port;
			break;
		case 8:
			return PWR_EN8_GPIO_Port;
			break;
		case 9:
			return PWR_EN9_GPIO_Port;
			break;

	}
}

inline uint16_t ldo_en_pin(uint16_t x)
{
	switch(x)
	{
		case 0:
			return PWR_EN0_Pin;
			break;
		case 1:
			return PWR_EN1_Pin;
			break;
		case 2:
			return PWR_EN2_Pin;
			break;
		case 3:
			return PWR_EN3_Pin;
			break;
		case 4:
			return PWR_EN4_Pin;
			break;
		case 5:
			return PWR_EN5_Pin;
			break;
		case 6:
			return PWR_EN6_Pin;
			break;
		case 7:
			return PWR_EN7_Pin;
			break;
		case 8:
			return PWR_EN8_Pin;
			break;
		case 9:
			return PWR_EN9_Pin;
			break;

	}
}

void bsp_ldo_init()
{
	int i=0;
	//HAL_GPIO_WritePin(ldo_port(0), ldo_en_pin(0) , GPIO_PIN_SET);
	//HAL_GPIO_WritePin(ldo_port(0), ldo_en_pin(0) , GPIO_PIN_RESET);

	for( i =0; i< MAX_LDO_COUNT ; i++)
	{
		HAL_GPIO_WritePin(ldo_port(i), ldo_en_pin(i) , GPIO_PIN_SET);


	}


}

void bsp_ldo_deinit()
{
	int i=0;
	for( i =0; i< MAX_LDO_COUNT ; i++)
	{
		HAL_GPIO_WritePin(ldo_port(i), ldo_en_pin(i) , GPIO_PIN_RESET);
		/*
		if( i == 0)
		   HAL_GPIO_WritePin(ldo_port(i), ldo_en_pin(i) , GPIO_PIN_SET);
		else
			HAL_GPIO_WritePin(ldo_port(i), ldo_en_pin(i) , GPIO_PIN_RESET);
			*/

	}
}

void bsp_ldo_off(int index)
{
	if(index < 0 || index >= MAX_LDO_COUNT)
		return;
	HAL_GPIO_WritePin(ldo_port(index) , ldo_en_pin(index) , GPIO_PIN_RESET);


}


void bsp_ldo_on(int index)
{

	if(index < 0 || index >= MAX_LDO_COUNT)
			return;
	HAL_GPIO_WritePin(ldo_port(index) , ldo_en_pin(index) , GPIO_PIN_SET);
}


/*
 * bsp.h
 *
 *  Created on: 2018. 6. 18.
 *      Author: jhpark
 */

#ifndef BSP_BSP_H_
#define BSP_BSP_H_



#include "stm32f1xx_hal.h"

#define SW_I2C_CLK_400KHz		1
#define SW_I2C_CLK_200KHz		10
#define SW_I2C_CLK_100KHz		25
#define SW_I2C_CLK_70KHz		40
#define SW_I2C_CLK_50KHz		65

#define SW_I2C                  0
#define STM_I2C					1

#define MSG_LENGTH     50

#define INDEX_MAX     99
#define SUB_INDEX_MAX   1




#define DATA_START_INDEX      3
#define CRC_START_INDEX       MSG_LENGTH - 2

#define ADC_COUNT     10
#define ADC_BUF_SIZE  ADC_COUNT*2


#define I2C_OK     0x0
#define I2C_BUSY   0x1
#define I2C_ERROR    0x2

#define MAX_PACKET_LENGTH   50

#define CO2       0x0
#define REF       0x1


#define zmdi_setting  0x10
#define read_Sensor_filter   0x20
#define read_Sensor_No_filter      0x30
#define zmdi_write_id       0x40
#define i2c_addr_write      0x50
#define i2c_scan            0x60



#define ToByte(a , b)      ((a >> (b*8)) & 0xff)
#define ToUInt(a,b,c,d)		( (a & 0xff) << 24 | (b & 0xff) << 16 | (c & 0xff) << 8 | (d & 0xff))
#define ToUShort( a , b)    ( (a & 0xff) << 8 | (b & 0xff))


int8_t bsp_sw_i2c_delay_test(uint32_t delay);
int8_t bsp_sw_i2c_set_addr_clock(uint8_t addr, uint16_t clock);
int8_t bsp_sw_i2c_init( uint8_t addr, uint8_t clock);
uint8_t bsp_sw_i2c_get_addr();
void bsp_sw_i2c_deinit();
uint8_t bsp_sw_i2c_ReadByte ( uint8_t ack);
uint8_t bsp_sw_i2c_WriteByte (uint8_t txByte);
void bsp_sw_i2c_StopCondition ( );
void bsp_sw_i2c_RepeatedStartCondition (  );
void bsp_sw_i2c_StartCondition (  );
void delay_us(uint32_t delay);
uint16_t get_i2c_clk(uint8_t clock);
int8_t bsp_i2c_write_sw_i2c(uint8_t* pData, uint16_t Size);
int8_t bsp_i2c_read_sw_i2c( uint8_t* pData, uint16_t Size);
HAL_StatusTypeDef bsp_sw_i2c_status();


void bsp_ldo_init();
void bsp_ldo_deinit();
void bsp_ldo_off(int index);
void bsp_ldo_on(int index);
int8_t bsp_tempus_init();
uint8_t bsp_mode_write(uint8_t cmd);
void bsp_write_register(uint8_t addr, uint8_t dat1 , uint8_t dat2);
void bsp_read_mem_register(uint8_t addr);


void bsp_read_filter();
void bsp_read_no_filter();
void init_Setting();
void write_id(uint16_t id);
uint16_t read_id();
void read_Setting();
uint8_t bsp_i2c_scan();
void bsp_i2c_addr_write(uint8_t addr);


int8_t bsp_i2c_init( uint8_t _i2c_addr);
int8_t bsp_i2c_write(uint8_t* pData, uint16_t Size);
int8_t bsp_i2c_read( uint8_t* pData, uint16_t Size);

char FloatHigh(float m_data);
char FloatLow(float m_data);
uint16_t get_crc16(uint8_t* temp, uint8_t len);

void bsp_mux_en_init();
void bsp_mux_en_deinit();
void bsp_set_mux_for_sensor(uint16_t id , uint16_t sub_id);

void copy_adc_value(uint8_t* dist);


#endif /* BSP_BSP_H_ */

/*
 * bsp_sw_i2c.c
 *
 *  Created on: 2018. 6. 18.
 *      Author: jhpark
 */


#include "main.h"
#include "stm32f1xx_hal.h"

#include "bsp.h"

#define MIN_SW_I2C_PORT_NUM		1

static uint8_t sw_i2c_addr = 0xff;
static uint8_t sw_i2c_clk = 0xff;

#define I2C_CH_SCK_PORT(x)     HAL_GPIO_WritePin(SW_I2C_SCL_GPIO_Port, SW_I2C_SCL_Pin, x)
#define I2C_CH_SDA_PORT(x)     HAL_GPIO_WritePin(SW_I2C_SDA_GPIO_Port, SW_I2C_SDA_Pin, x)
#define I2C_CH_SDA_READ        HAL_GPIO_ReadPin(SW_I2C_SDA_GPIO_Port, SW_I2C_SDA_Pin)
#define I2C_CH_SCK_READ        HAL_GPIO_ReadPin(SW_I2C_SCL_GPIO_Port , SW_I2C_SCL_Pin)


static void oSCK(  GPIO_PinState level )
{

	I2C_CH_SCK_PORT(level);

}

static void oSDA( GPIO_PinState level )
{

	I2C_CH_SDA_PORT(level);

}

static uint8_t iSDA( )
{
	unsigned char retValue=0;

    retValue = I2C_CH_SDA_READ;
    return retValue;

}
static uint8_t iSCL( )
{
	unsigned char retValue=0;

    retValue = I2C_CH_SCK_READ;
    return retValue;

}

static void SDA_MODE_IN()
{
    GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_TypeDef* port = NULL;
	uint32_t pin = 0;


	pin = SW_I2C_SDA_Pin;
	port = SW_I2C_SDA_GPIO_Port;


	GPIO_InitStruct.Pin = pin;

	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;

	HAL_GPIO_Init(port, &GPIO_InitStruct);
}

static void SDA_MODE_OUT()
{
    GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_TypeDef* port = NULL;
	uint32_t pin = 0;

	pin = SW_I2C_SDA_Pin;
	port = SW_I2C_SDA_GPIO_Port;


	GPIO_InitStruct.Pin = pin;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;

	HAL_GPIO_Init(port, &GPIO_InitStruct);
}

static void SCL_MODE_IN()
{
    GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_TypeDef* port = NULL;
	uint32_t pin = 0;


	pin = SW_I2C_SCL_Pin;
	port = SW_I2C_SCL_GPIO_Port;


	GPIO_InitStruct.Pin = pin;

	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;

	HAL_GPIO_Init(port, &GPIO_InitStruct);
}

static void SCL_MODE_OUT()
{
    GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_TypeDef* port = NULL;
	uint32_t pin = 0;

	pin = SW_I2C_SCL_Pin;
	port = SW_I2C_SCL_GPIO_Port;


	GPIO_InitStruct.Pin = pin;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;

	HAL_GPIO_Init(port, &GPIO_InitStruct);
}

void delay_us(uint32_t delay)
{
	uint32_t count = delay*8-2;
	while(delay--)
	{
		asm("nop");
	}
}

static int8_t check_channel_number()
{


	return 0;
}

int8_t bsp_sw_i2c_set_addr_clock(uint8_t addr, uint16_t clock)
{
	sw_i2c_addr = addr<<1;
	sw_i2c_clk = clock;
	//LOG("You specified sw i2c addr 0x%x with %d KHz\n", addr, clock);

	return I2C_OK;
}

int8_t bsp_sw_i2c_init(uint8_t addr, uint8_t clock)
{
	sw_i2c_addr = addr<<1;
	sw_i2c_clk = clock;

	return I2C_OK;
}
uint8_t bsp_sw_i2c_get_addr()
{
	uint8_t ret_i2c_addr = sw_i2c_addr >> 1;
	return ret_i2c_addr;
}
void bsp_sw_i2c_deinit()
{
	 GPIO_InitTypeDef GPIO_InitStruct;
	 GPIO_TypeDef* port = NULL;

	 GPIO_InitTypeDef GPIO_InitStruct_2;
	 GPIO_TypeDef* port_2 = NULL;

	 uint32_t pin = 0;
	 uint32_t pin_2 = 0;

	 pin = SW_I2C_SDA_Pin;
	 port = SW_I2C_SDA_GPIO_Port;

	 pin_2 = SW_I2C_SCL_Pin;
	 port_2 = SW_I2C_SCL_GPIO_Port;


	 GPIO_InitStruct.Pin = SW_I2C_SDA_Pin;
	 GPIO_InitStruct.Pull = GPIO_PULLUP;
	 GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	 GPIO_InitStruct.Mode = GPIO_MODE_INPUT;

	 GPIO_InitStruct_2.Pin = SW_I2C_SCL_Pin;
	 GPIO_InitStruct_2.Pull = GPIO_PULLUP;
	 GPIO_InitStruct_2.Speed = GPIO_SPEED_FREQ_HIGH;
	 GPIO_InitStruct_2.Mode = GPIO_MODE_INPUT;


	 HAL_GPIO_Init(SW_I2C_SDA_GPIO_Port , &GPIO_InitStruct);
	 HAL_GPIO_Init(SW_I2C_SCL_GPIO_Port , &GPIO_InitStruct);

	 //HAL_GPIO_DeInit(SW_I2C_SDA_GPIO_Port , SW_I2C_SDA_Pin);
	// HAL_GPIO_DeInit(SW_I2C_SCL_GPIO_Port , SW_I2C_SCL_Pin);

}
void bsp_sw_i2c_StartCondition ()
{
	//SCL_MODE_OUT();
	//SDA_MODE_OUT();
	//delay_us(sw_i2c_clk);

	oSDA(GPIO_PIN_SET );
	delay_us(sw_i2c_clk);
	oSCK( GPIO_PIN_SET);
	delay_us(sw_i2c_clk);
	oSDA( GPIO_PIN_RESET );
	delay_us(sw_i2c_clk);  // hold time start condition (t_HD;STA)
	oSCK( GPIO_PIN_RESET );
	delay_us(sw_i2c_clk);
}

void bsp_sw_i2c_RepeatedStartCondition ()
{
	oSDA( GPIO_PIN_SET );
	delay_us(sw_i2c_clk);
	oSCK( GPIO_PIN_SET);
	delay_us(sw_i2c_clk);
	oSCK( GPIO_PIN_RESET );
	delay_us(sw_i2c_clk);  // hold time start condition (t_HD;STA)
	oSCK( GPIO_PIN_SET );
    delay_us(sw_i2c_clk);
	oSDA( GPIO_PIN_RESET );
	delay_us(sw_i2c_clk);
    oSCK( GPIO_PIN_RESET );
    delay_us(sw_i2c_clk);
}

void bsp_sw_i2c_StopCondition ()
{
	oSDA( GPIO_PIN_RESET);
	delay_us(sw_i2c_clk);
	oSCK( GPIO_PIN_RESET );
	delay_us(sw_i2c_clk);
	oSCK( GPIO_PIN_SET );
	delay_us(sw_i2c_clk);  // set-up time stop condition (t_SU;STO)
	oSDA( GPIO_PIN_SET );
	delay_us(sw_i2c_clk);
	//SCL_MODE_IN();
	//SDA_MODE_IN();

}

uint8_t bsp_sw_i2c_WriteByte ( uint8_t txByte)
{
	uint8_t mask,error=0;

	for (mask=0x80; mask>0; mask>>=1)   //shift bit for masking (8 times)
	{
		if ((mask & txByte) == 0)
			oSDA( GPIO_PIN_RESET );//masking txByte, write bit to SDA-Line
		else
			oSDA(  GPIO_PIN_SET );
		delay_us(sw_i2c_clk/2);             //data set-up time (t_SU;DAT)
		oSCK(  GPIO_PIN_SET );                         //generate clock pulse on SCL
		delay_us(sw_i2c_clk);             //SCL high time (t_HIGH)
		oSCK(  GPIO_PIN_RESET );
		delay_us(sw_i2c_clk/2);             //data hold time(t_HD;DAT)
	}

	//oSDA( channel,  GPIO_PIN_SET );                          //release SDA-line
    //oSDA(  GPIO_PIN_SET );                          //release SDA-line

	SDA_MODE_IN();
	delay_us(sw_i2c_clk/2);

	oSCK(   GPIO_PIN_SET );	                   //clk #9 for ack
	delay_us(sw_i2c_clk);               		//data set-up time (t_SU;DAT)

	if( iSDA() == GPIO_PIN_SET )
	{
		//ERR("I2C NACK...0x%x...%d!!!\n", txByte, channel);
		error = I2C_ERROR; //check ack from i2c slave
	}
	oSCK( GPIO_PIN_RESET );
	delay_us(sw_i2c_clk);

	SDA_MODE_OUT();//wait time to see byte package on scope


	return error;                       //return error code
}

uint8_t bsp_sw_i2c_ReadByte ( uint8_t ack)
{
	uint8_t mask,rxByte=0;

	//oSDA( channel,  GPIO_PIN_SET );                           //release SDA-line
	oSDA( GPIO_PIN_RESET );                           //release SDA-line

	SDA_MODE_IN();
  	delay_us(sw_i2c_clk);
	for (mask=0x80; mask>0; mask>>=1)   //shift bit for masking (8 times)
	{
		oSCK(  GPIO_PIN_SET );                         //start clock on SCL-line
		delay_us(sw_i2c_clk/2);             //data set-up time (t_SU;DAT)
				             //SCL high time (t_HIGH)
		if ( iSDA() ==1 )
			rxByte=(rxByte | mask); //read bit
		delay_us(sw_i2c_clk/2);

		oSCK( GPIO_PIN_RESET );
		delay_us(sw_i2c_clk);             //data hold time(t_HD;DAT)
	}

	SDA_MODE_OUT();
	oSDA(  (GPIO_PinState)ack );	//send acknowledge if necessary
	delay_us(sw_i2c_clk);               //data set-up time (t_SU;DAT)
	oSCK( GPIO_PIN_SET );                           //clk #9 for ack
	delay_us(sw_i2c_clk);               //SCL high time (t_HIGH)
	oSCK( GPIO_PIN_RESET );
	delay_us(sw_i2c_clk);
	//oSDA( channel,  GPIO_PIN_SET );            //release SDA-line
	//delay_us(sw_i2c_clk);              //wait time to see byte package on scope
	return rxByte;                      //return error code

}
uint16_t get_i2c_clk(uint8_t clock)
{
	uint16_t clk = 0;

	switch(clock)
	{
		case SW_I2C_CLK_400KHz:	clk = 400;
			break;

		case SW_I2C_CLK_200KHz:	clk = 200;
			break;

		case SW_I2C_CLK_100KHz:	clk = 100;
			break;

		case SW_I2C_CLK_70KHz:	clk = 70;
			break;

		case SW_I2C_CLK_50KHz:	clk = 50;
			break;

		default:
			break;
	}
	return clk;
}

int8_t bsp_sw_i2c_delay_test(uint32_t delay)
{
	int8_t i;

	for(i = 0; i < 10; i++)
	{
		oSCK(GPIO_PIN_RESET);
		delay_us(delay);
		oSCK(GPIO_PIN_SET);
		delay_us(delay);
	}
	return 0;
}

int8_t bsp_i2c_write_sw_i2c(uint8_t* pData, uint16_t Size)
{
	uint8_t i2c_addr_temporary;
	int8_t ret;
	uint16_t i = 0;
	i2c_addr_temporary = sw_i2c_addr;
	bsp_sw_i2c_StartCondition();
	ret = bsp_sw_i2c_WriteByte(i2c_addr_temporary);

	for( i = 0; i< Size ; i++){
		ret |= bsp_sw_i2c_WriteByte(pData[i]);
	}
	bsp_sw_i2c_StopCondition();



	return ret;
}

int8_t bsp_i2c_read_sw_i2c( uint8_t* pData, uint16_t Size)
{
	uint16_t i;
	uint8_t i2c_addr_temporary;
	int8_t ret;

	i2c_addr_temporary = sw_i2c_addr | 0x1;

	bsp_sw_i2c_StartCondition();
	ret = bsp_sw_i2c_WriteByte( i2c_addr_temporary);

	for(i = 0; i < Size-1; i++)
	{
		pData[i] = bsp_sw_i2c_ReadByte(0);
	}
	pData[i] = bsp_sw_i2c_ReadByte(1);
	bsp_sw_i2c_StopCondition ();

	return ret;
}

HAL_StatusTypeDef bsp_sw_i2c_status()
{
	SCL_MODE_IN();
	SDA_MODE_IN();

	HAL_StatusTypeDef status;
	osDelay(1);


	if(iSCL() == GPIO_PIN_RESET || iSDA() == GPIO_PIN_RESET){
		status = HAL_ERROR;
	} else{
		status = HAL_OK;
	}


	//SCL_MODE_OUT();
	//SDA_MODE_OUT();
	osDelay(1);

	return status;
}



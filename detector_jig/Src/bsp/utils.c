/*
 * utils.c
 *
 *  Created on: 2018. 6. 18.
 *      Author: jhpark
 */

#include <stdint.h>
uint16_t get_crc16(uint8_t* temp, uint8_t len)
{
	uint16_t CRC16, POLY;
	uint8_t MessageLength;
	int shift = 0;

	CRC16 = 0xffff;
	POLY = 0xa001;
	MessageLength = len;

	for (int i = MessageLength - 1; i >= 0; i--)
	{
		unsigned short code = (unsigned short)(0xff & temp[MessageLength - 1 - i]);
		CRC16 = CRC16^ code;
		shift = 0;
		while (shift <= 7) {
			if (CRC16 & 0x1) {
				CRC16 = CRC16 >> 1;
				CRC16 = CRC16 ^ POLY;
			}
			else {
				CRC16 = CRC16 >> 1;
			}
			shift++;
		}
	}
	return CRC16;
}

char FloatHigh(float m_data)
{
    char retValue = 0;

    retValue = (char)m_data;
    return retValue;
}

char FloatLow(float m_data)
{
    char retValue = 0;
    retValue = (char)((int)(m_data * 100) % 100);
    if (retValue < 0) retValue = retValue * (-1);

    return retValue;
}



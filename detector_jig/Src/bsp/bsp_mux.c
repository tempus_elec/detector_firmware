/*
 * bsp_mux.c
 *
 *  Created on: 2018. 6. 18.
 *      Author: jhpark
 */

#include <stdint.h>
#include <stdbool.h>
#include "string.h"
#include "bsp.h"
#include "bsp_mux.h"
#include "main.h"

inline GPIO_TypeDef * mux_port(uint16_t x)
{
	switch(x)
	{
	case 0:
		return MUX_EN0_GPIO_Port;
		break;
	case 1:
		return MUX_EN1_GPIO_Port;
		break;
	case 2:
		return MUX_EN2_GPIO_Port;
		break;
	case 3:
		return MUX_EN3_GPIO_Port;
		break;
	case 4:
		return MUX_EN4_GPIO_Port;
		break;
	case 5:
		return MUX_EN5_GPIO_Port;
		break;
	case 6:
		return MUX_EN6_GPIO_Port;
		break;
	case 7:
		return MUX_EN7_GPIO_Port;
		break;
	case 8:
		return MUX_EN8_GPIO_Port;
		break;
	case 9:
		return MUX_EN9_GPIO_Port;
		break;
	case 10:
		return MUX_EN10_GPIO_Port;
		break;
	case 11:
		return MUX_EN11_GPIO_Port;
		break;
	case 12:
		return MUX_EN12_GPIO_Port;
		break;
	case 13:
		return MUX_EN13_GPIO_Port;
		break;
	case 14:
		return MUX_EN14_GPIO_Port;
		break;
	case 15:
		return MUX_EN15_GPIO_Port;
		break;
	case 16:
		return MUX_EN16_GPIO_Port;
		break;
	case 17:
		return MUX_EN17_GPIO_Port;
		break;
	case 18:
		return MUX_EN18_GPIO_Port;
		break;
	case 19:
		return MUX_EN19_GPIO_Port;
		break;

	}
}

inline uint16_t mux_en_pin(uint16_t x)
{
	switch(x)
	{
		case 0:
			return MUX_EN0_Pin;
			break;
		case 1:
			return MUX_EN1_Pin;
			break;
		case 2:
			return MUX_EN2_Pin;
			break;
		case 3:
			return MUX_EN3_Pin;
			break;
		case 4:
			return MUX_EN4_Pin;
			break;
		case 5:
			return MUX_EN5_Pin;
			break;
		case 6:
			return MUX_EN6_Pin;
			break;
		case 7:
			return MUX_EN7_Pin;
			break;
		case 8:
			return MUX_EN8_Pin;
			break;
		case 9:
			return MUX_EN9_Pin;
			break;
		case 10:
			return MUX_EN10_Pin;
			break;
		case 11:
			return MUX_EN11_Pin;
			break;
		case 12:
			return MUX_EN12_Pin;
			break;
		case 13:
			return MUX_EN13_Pin;
			break;
		case 14:
			return MUX_EN14_Pin;
			break;
		case 15:
			return MUX_EN15_Pin;
			break;
		case 16:
			return MUX_EN16_Pin;
			break;
		case 17:
			return MUX_EN17_Pin;
			break;
		case 18:
			return MUX_EN18_Pin;
			break;
		case 19:
			return MUX_EN19_Pin;
			break;


	}
}

void bsp_set_mux_enable(uint16_t x)
{
	if( x >= MUX_COUNT)
		return;

	int i=0;
	for( i = 0; i < MUX_COUNT ; i++ )
	{
		if ( i == x )
			HAL_GPIO_WritePin(mux_port(i),mux_en_pin(i), GPIO_PIN_RESET);
		else
			HAL_GPIO_WritePin(mux_port(i),mux_en_pin(i), GPIO_PIN_SET);
	}

}

void bsp_set_mux_channel(uint16_t x)
{
	if( x >= MUX_CHANNLE_COUNT)
		return;

	(x & S0) ? \
		HAL_GPIO_WritePin(MUX_S0_GPIO_Port, MUX_S0_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(MUX_S0_GPIO_Port, MUX_S0_Pin, GPIO_PIN_RESET);
	(x & S1) ? \
		HAL_GPIO_WritePin(MUX_S1_GPIO_Port, MUX_S1_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(MUX_S1_GPIO_Port, MUX_S1_Pin, GPIO_PIN_RESET);
	(x & S2) ? \
		HAL_GPIO_WritePin(MUX_S2_GPIO_Port, MUX_S2_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(MUX_S2_GPIO_Port, MUX_S2_Pin, GPIO_PIN_RESET);
	(x & S3) ? \
		HAL_GPIO_WritePin(MUX_S3_GPIO_Port, MUX_S3_Pin, GPIO_PIN_SET) : HAL_GPIO_WritePin(MUX_S3_GPIO_Port, MUX_S3_Pin, GPIO_PIN_RESET);

}

void bsp_set_mux_for_sensor(uint16_t id , uint16_t sub_id)
{

	uint16_t mux_en_id = (id / 10);
	uint16_t S_id = id % 10;
	uint16_t muxport = 0;

	switch(sub_id){
	case 0:
		muxport = (mux_en_id * 2) + 1; // co2
		break;
	default:
		muxport =( mux_en_id * 2);  //ref
		break;
	}

	bsp_set_mux_enable(muxport);
	osDelay(3);
	bsp_set_mux_channel(S_id);
	osDelay(3);
}

void bsp_mux_en_init(){
	int i=0;
	for( i = 0; i < MUX_COUNT ; i++ )
	{
			HAL_GPIO_WritePin(mux_port(i),mux_en_pin(i), GPIO_PIN_RESET);
	}
}
void bsp_mux_en_deinit(){
	int i=0;
	for( i = 0; i < MUX_COUNT ; i++ )
	{
			HAL_GPIO_WritePin(mux_port(i),mux_en_pin(i), GPIO_PIN_SET);
	}
}




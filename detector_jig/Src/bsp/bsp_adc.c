/*
 * bsp_adc.c
 *
 *  Created on: 2018. 6. 18.
 *      Author: jhpark
 */


#include "bsp.h"
uint16_t adc_buf[ADC_COUNT];

void copy_adc_value(uint8_t* dist)
{
	uint8_t dist_index = 0;
	for(int i=0;i<ADC_COUNT;i++){
		dist[dist_index++] = ToByte(adc_buf[i] , 0);
		dist[dist_index++] = ToByte(adc_buf[i] , 1);
	}

}

/*
 * bsp_tempus.c
 *
 *  Created on: 2018. 6. 18.
 *      Author: jhpark
 */



#include "bsp.h"
#include <stdint.h>
#include "bsp_tempus.h"


#define i2c_delay    0x1

typedef int8_t (*p_i2c_read)(uint8_t* , uint16_t);
typedef int8_t (*p_i2c_write)(uint8_t*, uint16_t);

p_i2c_read tempus_i2c_read = NULL;
p_i2c_write tempus_i2c_write = NULL;

TEMPUS_ST g_Tempus = {0};

uint8_t tempus_buf[TEMPUS_BUF_SIZE] = {0};
static uint32_t ABS_Gain_T = Gain_T;
uint32_t ABS_SOT_T = SOT_T;
uint32_t ABS_Offset_T = Offset_T;
uint32_t ABS_Gain_B = Gain_B;

static void check_busy(void);



int8_t bsp_tempus_init()
{
	int8_t ret;
	ret = bsp_sw_i2c_set_addr_clock(I2C_ADDR_TEMPUS, 750);
	tempus_i2c_read = &bsp_i2c_read_sw_i2c;
	tempus_i2c_write = &bsp_i2c_write_sw_i2c;

    return ret;
}
void bsp_tempus_i2c_addr_set(uint8_t addr)
{
	bsp_sw_i2c_set_addr_clock(addr, 750);
}

static void check_busy(void)
{
    int i = 0;
    int j= 0 ;
    int8_t ret;
    uint8_t busy = 0;
    while(1)
    {
        //tempus_read(&busy, 1);
    	ret = tempus_i2c_read(&busy , 1);
    	if(ret == I2C_ERROR)
    	{
    		g_Tempus.status = I2C_ERROR;
    		break;
    	}


        if ((busy & 0x20) == 0)
        {
        	g_Tempus.status = I2C_OK;
            break;
        }
        osDelay(1);
        //delay_us(5);

        i++;
        if (i > 15)
        {
        	g_Tempus.status = I2C_BUSY;
            //LOG("Tempus busy check error!!!\n");
            break;
        }
    }


}
void bsp_write_register( uint8_t addr , uint8_t dat1 , uint8_t dat2)
{
	uint8_t write_dat[3];
	int8_t ret;
	write_dat[0] = addr + 0x40;
	write_dat[1] = dat1;
	write_dat[2] = dat2;


	ret = tempus_i2c_write(write_dat , 3);
	osDelay(i2c_delay);
	check_busy();
	osDelay(i2c_delay);

}

void bsp_read_mem_register(uint8_t addr)
{
	uint8_t write_dat[3];
	uint8_t read_dat[3];
	int8_t ret;
	write_dat[0] = addr;
	write_dat[1] = 0x80;
	write_dat[2] = 0xa9;

	ret = tempus_i2c_write(write_dat , 3);
	osDelay(i2c_delay);
	check_busy();
	osDelay(i2c_delay);
	ret = tempus_i2c_read(read_dat , 3);
	osDelay(i2c_delay);

	g_Tempus.mem1 = read_dat[1];
	g_Tempus.mem2 = read_dat[2];



}
uint8_t bsp_mode_write(uint8_t cmd)
{
	int8_t ret;
	uint8_t write_dat = cmd;
	ret = tempus_i2c_write(&write_dat , 1);
	osDelay(i2c_delay);
	check_busy();
	osDelay(i2c_delay);
}

void bsp_read_filter()
{
	int8_t ret = 0x0;
	int8_t b_ret = 0x0;
	int8_t t_ret = 0x0;

	uint8_t write_dat[3];
	uint32_t b_raw[15] = {0,};
	uint32_t t_raw[15] = {0,};

	uint32_t b_raw_avg = 0;
	uint32_t b_raw_avg_Count = 0;
	uint32_t t_raw_avg = 0;
	uint32_t t_raw_avg_Count = 0;

	int dwCount = 0;


	write_dat[0] = CMD_MEASURE;
	write_dat[1] = 0x0;
	write_dat[2] = 0x0;

	uint8_t read_dat[7];
	bsp_mode_write(Start_CM);
	osDelay(1);
#if 0

	ret = tempus_i2c_write(write_dat , 3);
	osDelay(i2c_delay);
	check_busy();
	osDelay(i2c_delay);
	tempus_i2c_read(read_dat , 7);
	osDelay(1);


	if(g_Tempus.status != I2C_OK && g_Tempus.status != I2C_BUSY){

		    g_Tempus.sensor_high = 0x0;
	    	g_Tempus.sensor_mid = 0x0;
	    	g_Tempus.sensor_low = 0x0;

	    	g_Tempus.temp_high = 0x0;
	    	g_Tempus.temp_mid = 0x0;
	    	g_Tempus.temp_low = 0x0;

	 } else{

	    	g_Tempus.sensor_high = read_dat[1];
	    	g_Tempus.sensor_mid =  read_dat[2];
	    	g_Tempus.sensor_low = read_dat[3];

	    	g_Tempus.temp_high =  read_dat[4];
	    	g_Tempus.temp_mid =  read_dat[5];
	    	g_Tempus.temp_low =  read_dat[6];

	  }
#endif

#if 1

	do{

		ret = tempus_i2c_write(write_dat , 3);
		osDelay(i2c_delay);
		check_busy();
		osDelay(i2c_delay);
		tempus_i2c_read(read_dat , 7);
		osDelay(1);

		b_raw[dwCount] = ToUInt( 0x0 , read_dat[1] , read_dat[2] , read_dat[3]);
		t_raw[dwCount] = ToUInt( 0x0 , read_dat[4] , read_dat[5] , read_dat[6]);
		dwCount++;
		osDelay(1);


	}while(g_Tempus.status == I2C_OK && dwCount < 12);

	if(dwCount < 12){
		ret = 0;

	} else {
		ret = 0;
		for(int i=7;i<11;i++){
			if(b_raw[i] != b_raw[i+1])
				b_ret++;
			if(t_raw[i] != t_raw[i+1])
				t_ret++;
		}

		for(int i=7;i<12;i++){
			if( b_raw[i] != 16777215 && b_raw[i] != 0){
				b_raw_avg += b_raw[i];
				b_raw_avg_Count++;
			}
			if( t_raw[i] != 16777215 && t_raw[i] != 0){
				t_raw_avg += t_raw[i];
				t_raw_avg_Count++;
			}
		}

		if(b_ret == 0 || t_ret == 0){
			ret = 0;
			g_Tempus.status = I2C_ERROR;
		}
		else
			ret++;



		if(b_raw_avg_Count == 0 || t_raw_avg_Count == 0){
			ret = 0;
			g_Tempus.status = I2C_ERROR;
		}


	}


    if(ret == 0){


    	g_Tempus.sensor_high = 0x0;
    	g_Tempus.sensor_mid = 0x0;
    	g_Tempus.sensor_low = 0x0;

    	g_Tempus.temp_high = 0x0;
    	g_Tempus.temp_mid = 0x0;
    	g_Tempus.temp_low = 0x0;

    } else {

    	b_raw_avg /= b_raw_avg_Count;
    	t_raw_avg /= t_raw_avg_Count;

    	g_Tempus.sensor_high = ToByte(b_raw_avg , 2);
    	g_Tempus.sensor_mid =  ToByte(b_raw_avg , 1);
    	g_Tempus.sensor_low = ToByte(b_raw_avg , 0);

    	g_Tempus.temp_high =  ToByte(t_raw_avg , 2);
    	g_Tempus.temp_mid =  ToByte(t_raw_avg , 1);
    	g_Tempus.temp_low =  ToByte(t_raw_avg , 0);

    }
#endif

	return ;

}
void bsp_read_no_filter()
{
	int8_t ret = 0x0;
	int8_t b_ret = 0x0;
	int8_t t_ret = 0x0;

	uint8_t write_dat[3];
	uint32_t b_raw[15] = {0,};
	uint32_t t_raw[15] = {0,};

	uint32_t b_raw_avg = 0;
	uint32_t b_raw_avg_Count = 0;
	uint32_t t_raw_avg = 0;
	uint32_t t_raw_avg_Count = 0;


	int dwCount = 0;


	write_dat[0] = CMD_MEASURE;
	write_dat[1] = 0x0;
	write_dat[2] = 0x0;

	uint8_t read_dat[7];
	bsp_mode_write(Start_CM);
	osDelay(1);


	ret = tempus_i2c_write(write_dat , 3);
	osDelay(i2c_delay);
	check_busy();
	osDelay(i2c_delay);
	tempus_i2c_read(read_dat , 7);
	osDelay(1);


	if(g_Tempus.status != I2C_OK && g_Tempus.status != I2C_BUSY){

		g_Tempus.sensor_high = 0x0;
		g_Tempus.sensor_mid = 0x0;
		g_Tempus.sensor_low = 0x0;

		g_Tempus.temp_high = 0x0;
		g_Tempus.temp_mid = 0x0;
		g_Tempus.temp_low = 0x0;

	} else{

		g_Tempus.sensor_high = read_dat[1];
		g_Tempus.sensor_mid =  read_dat[2];
		g_Tempus.sensor_low = read_dat[3];

		g_Tempus.temp_high =  read_dat[4];
		g_Tempus.temp_mid =  read_dat[5];
		g_Tempus.temp_low =  read_dat[6];

	}
}

void init_Setting()
{
	uint32_t ABS_Gain_T = Gain_T;
	uint32_t ABS_SOT_T = SOT_T;
	uint32_t ABS_Offset_T = Offset_T;
	uint32_t ABS_Gain_B = Gain_B;

	bsp_mode_write(Start_CM);
	osDelay(1);

	g_Tempus.mem1 = 0xeb, g_Tempus.mem2 = 0x3e;
	bsp_write_register(0x12 , g_Tempus.mem1 , g_Tempus.mem2);
	if(g_Tempus.status != I2C_OK)
		return;
	osDelay(1);

	g_Tempus.mem1 = 0x00, g_Tempus.mem2 = 0x00;
	bsp_write_register(0x09, g_Tempus.mem1 , g_Tempus.mem2);
	if(g_Tempus.status != I2C_OK)
		return;
	osDelay(1);

	g_Tempus.mem1 = ToByte(ABS_Gain_T , 2), g_Tempus.mem2 = ToByte(ABS_SOT_T , 2);
	bsp_write_register(0x11, g_Tempus.mem1 , g_Tempus.mem2);
	if(g_Tempus.status != I2C_OK)
		return;
	osDelay(1);

	g_Tempus.mem1 = ToByte(ABS_Gain_T , 1), g_Tempus.mem2 = ToByte(ABS_Gain_T , 0);
	bsp_write_register(0x0B, g_Tempus.mem1 , g_Tempus.mem2);
	if(g_Tempus.status != I2C_OK)
		return;
	osDelay(1);

	g_Tempus.mem1 = ToByte(ABS_SOT_T , 1), g_Tempus.mem2 = ToByte(ABS_SOT_T , 0);
	bsp_write_register(0x0C, g_Tempus.mem1 , g_Tempus.mem2);
	if(g_Tempus.status != I2C_OK)
		return;
	osDelay(1);

	g_Tempus.mem1 = 0x00, g_Tempus.mem2 = ToByte(ABS_Offset_T , 2);
	bsp_write_register(0x10, g_Tempus.mem1 , g_Tempus.mem2);
	if(g_Tempus.status != I2C_OK)
		return;
	osDelay(1);

	g_Tempus.mem1 = ToByte(ABS_Offset_T , 1), g_Tempus.mem2 = ToByte(ABS_Offset_T , 0);
	bsp_write_register(0x0A, g_Tempus.mem1 , g_Tempus.mem2);
	if(g_Tempus.status != I2C_OK)
		return;
	osDelay(1);

	g_Tempus.mem1 = 0x1, g_Tempus.mem2 = ToByte(ABS_Gain_B , 2);
	bsp_write_register(0x0D, g_Tempus.mem1 , g_Tempus.mem2);
	if(g_Tempus.status != I2C_OK)
		return;
	osDelay(1);

	g_Tempus.mem1 = ToByte(ABS_Gain_B , 1), g_Tempus.mem2 = ToByte(ABS_Gain_B , 0);
	bsp_write_register(0x04, g_Tempus.mem1 , g_Tempus.mem2);
	if(g_Tempus.status != I2C_OK)
		return;
	osDelay(1);

	g_Tempus.mem1 = 0x0, g_Tempus.mem2 = 0x0;
	bsp_write_register(0x17, g_Tempus.mem1 , g_Tempus.mem2);
	if(g_Tempus.status != I2C_OK)
		return;
	osDelay(1);

	g_Tempus.mem1 = 0x0, g_Tempus.mem2 = 0x0;
	bsp_write_register(0x19, g_Tempus.mem1 , g_Tempus.mem2);
	if(g_Tempus.status != I2C_OK)
		return;
	osDelay(1);


	/*
	bsp_write_mem_reigster(0x0b ,  ToByte(ABS_Gain_T , 1) ,ToByte(ABS_Gain_T , 0) );
	osDelay(1);
	bsp_write_mem_reigster(0x0c ,  ToByte(ABS_SOT_T , 1) ,ToByte(ABS_SOT_T , 0) );
	osDelay(1);
	bsp_write_mem_reigster(0x10 ,  0x00 ,ToByte(ABS_Offset_T , 2) );
	osDelay(1);
	bsp_write_mem_reigster(0x0a ,  ToByte(ABS_Offset_T , 1) ,ToByte(ABS_Offset_T , 0) );
	osDelay(1);
	bsp_write_mem_reigster(0x0d ,  0x01 ,ToByte(ABS_Gain_B , 2) );
	osDelay(1);
	*/
	/*
	bsp_write_mem_reigster(0x04 ,  ToByte(ABS_Gain_B , 1) ,ToByte(ABS_Gain_B , 0) );
	osDelay(1);
	bsp_write_mem_reigster(0x17 ,  0x00  ,0x00 );
	osDelay(1);
	bsp_write_mem_reigster(0x19 ,  0x00  ,0x00 );
	osDelay(1);
	*/


}
void read_Setting()
{

	uint8_t S_H,S_M,S_L;
	bsp_mode_write(Start_CM);
	osDelay(1);

	g_Tempus.mem1 = 0x00, g_Tempus.mem2 = 0x00;
	bsp_read_mem_register(0x10);
	if(g_Tempus.status != I2C_OK)
		return;

	S_H = g_Tempus.mem2;
	bsp_read_mem_register(0xA);
	if(g_Tempus.status != I2C_OK)
		return;
	S_M = g_Tempus.mem1;
	S_L = g_Tempus.mem2;
	g_Tempus.rev_Offset_T = S_H * 65536 + S_M * 256 + S_L;
	//g_Tempus.


	g_Tempus.mem1 = 0x00, g_Tempus.mem2 = 0x00;
	bsp_read_mem_register(0xD);
	if(g_Tempus.status != I2C_OK)
		return;
	S_H = g_Tempus.mem1;
	bsp_read_mem_register(0x3);
	if(g_Tempus.status != I2C_OK)
		return;
	S_M = g_Tempus.mem1;
	S_L = g_Tempus.mem2;
	g_Tempus.rev_Offset_B = S_H * 65536 + S_M * 256 + S_L;

	g_Tempus.mem1 = 0x00, g_Tempus.mem2 = 0x00;
	bsp_read_mem_register(0xD);
	if(g_Tempus.status != I2C_OK)
		return;
	S_H = g_Tempus.mem2;
	bsp_read_mem_register(0x4);
	if(g_Tempus.status != I2C_OK)
		return;
	S_M = g_Tempus.mem1;
	S_L = g_Tempus.mem2;
	g_Tempus.rev_Gain_B = S_H * 65536 + S_M * 256 + S_L;

	g_Tempus.mem1 = 0x00, g_Tempus.mem2 = 0x00;
	bsp_read_mem_register(0x10);
	if(g_Tempus.status != I2C_OK)
		return;
	S_H = g_Tempus.mem1;
	bsp_read_mem_register(0x9);
	if(g_Tempus.status != I2C_OK)
		return;
	S_M = g_Tempus.mem1;
	S_L = g_Tempus.mem2;
	g_Tempus.rev_SOT_B = S_H * 65536 + S_M * 256 + S_L;

	g_Tempus.mem1 = 0x00, g_Tempus.mem2 = 0x00;
	bsp_read_mem_register(0x19);
	if(g_Tempus.status != I2C_OK)
		return;
	S_H = g_Tempus.mem2;
	bsp_read_mem_register(0x17);
	if(g_Tempus.status != I2C_OK)
		return;
	S_M = g_Tempus.mem1;
	S_L = g_Tempus.mem2;
	g_Tempus.rev_Sens_Shift = S_H * 65536 + S_M * 256 + S_L;

	g_Tempus.mem1 = 0x00, g_Tempus.mem2 = 0x00;
	bsp_read_mem_register(0x11);
	if(g_Tempus.status != I2C_OK)
		return;
	S_H = g_Tempus.mem2;
	bsp_read_mem_register(0xC);
	if(g_Tempus.status != I2C_OK)
		return;
	S_M = g_Tempus.mem1;
	S_L = g_Tempus.mem2;
	g_Tempus.rev_SOT_T = S_H * 65536 + S_M * 256 + S_L;

	g_Tempus.mem1 = 0x00, g_Tempus.mem2 = 0x00;
	bsp_read_mem_register(0x11);
	if(g_Tempus.status != I2C_OK)
		return;
	S_H = g_Tempus.mem1;
	bsp_read_mem_register(0xB);
	if(g_Tempus.status != I2C_OK)
		return;
	S_M = g_Tempus.mem1;
	S_L = g_Tempus.mem2;
	g_Tempus.rev_Gain_T = S_H * 65536 + S_M * 256 + S_L;


}
void write_id( uint16_t id)
{
	uint8_t id_high = ToByte(id , 1);
	uint8_t id_low = ToByte(id , 0);


	bsp_mode_write(Start_CM);
	osDelay(1);

	/*
	for(int i=0;i<3;i++){
		bsp_write_register( 0x28 , id_high , id_low );
		osDelay(1);
	}
	*/
	bsp_write_register( 0x28 , id_high , id_low );


}
uint16_t read_id()
{
	bsp_mode_write(Start_CM);
	osDelay(1);
	bsp_read_mem_register(0x28);

	uint8_t id_high = g_Tempus.mem1;
	uint8_t id_low = g_Tempus.mem2;

	uint16_t ret = ToUShort( id_high , id_low );

	return ret;

}
uint8_t bsp_i2c_scan()
{
	uint8_t ret_addr = 0xff;
	uint8_t ret = 0x0;


	for(uint8_t i=0 ; i<0x80 ;i++){

		bsp_sw_i2c_set_addr_clock(i, 750);
		bsp_mode_write(Start_CM);

		if(g_Tempus.status == I2C_OK || g_Tempus.status == I2C_BUSY){
			ret_addr = bsp_sw_i2c_get_addr();
		  	break;

		}
	}


	return ret_addr;
}
void bsp_i2c_addr_write(uint8_t addr)
{
	uint8_t write_addr = addr;
	if( write_addr > 127)
		write_addr = 127;

	uint8_t backup = 0x0;
	bsp_mode_write(Start_CM);
	osDelay(1);
	bsp_read_mem_register(0x2);
	if( g_Tempus.status != I2C_OK)
		return;
	osDelay(1);
	backup = g_Tempus.mem2;
	g_Tempus.mem2 = (( backup & 0x80 )| write_addr);
	bsp_write_register(0x2 , g_Tempus.mem1 , g_Tempus.mem2);
	osDelay(1);



	return;


}

/*
static uint16_t get_i2c_clk(uint8_t clock)
{
	uint16_t clk = 0;

	switch(clock)
	{
		case SW_I2C_CLK_400KHz:	clk = 400;
			break;

		case SW_I2C_CLK_200KHz:	clk = 200;
			break;

		case SW_I2C_CLK_100KHz:	clk = 100;
			break;

		case SW_I2C_CLK_70KHz:	clk = 70;
			break;

		case SW_I2C_CLK_50KHz:	clk = 50;
			break;

		default:
			break;
	}
	return clk;
}

int8_t bsp_tempus_init(uint8_t i2c_flag)
{
	int8_t ret;

	ret = bsp_sw_i2c_set_addr_clock(I2C_ADDR_TEMPUS, get_i2c_clk(SW_I2C_CLK_100KHz));

	i2c_option = i2c_flag;


    return ret;
}

static int8_t tempus_write(uint8_t* buff, uint8_t size)
{
	int8_t ret = 0;
	uint8_t i;

	bsp_sw_i2c_StartCondition();
	bsp_sw_i2c_WriteByte(TEMPUS_I2C_ADR_W);

	for(i = 0; i < size; i++)
	{
		bsp_sw_i2c_WriteByte( buff[i]);
	}
	bsp_sw_i2c_StopCondition ();


	return ret;


}

static int8_t tempus_read(uint8_t* buff, uint8_t size)
{
	uint8_t i;

	bsp_sw_i2c_StartCondition();
	bsp_sw_i2c_WriteByte( TEMPUS_I2C_ADR_R);

	for(i = 0; i < size-1; i++)
	{
		buff[i] = bsp_sw_i2c_ReadByte(0);
	}
	buff[i] = bsp_sw_i2c_ReadByte(1);
	bsp_sw_i2c_StopCondition ();

	return 0;
}

static void check_busy(void)
{
    int i = 0;
    uint8_t busy = 0;
    while(1)
    {
        //tempus_read(&busy, 1);
        bsp_sw_i2c_StartCondition();
		bsp_sw_i2c_WriteByte(TEMPUS_I2C_ADR_R);
		busy = bsp_sw_i2c_ReadByte(1);
		bsp_sw_i2c_StopCondition ();

        if ((busy&0x20) ==0)
        {
            break;
        }
        //osDelay(3);
        delay_us(10);

        i++;
        if (i > 10)
        {
            //LOG("Tempus busy check error!!!\n");
            break;
        }
    }
}
*/
/*
int8_t bsp_tempus_read()
{
    char count = 0;
    uint32_t sum_sensor, sum_temp;

    sum_sensor = sum_temp = 0;

	//bsp_tempus_init();

    for(count = 0; count < TEMPUS_AVR_COUNT; count++)
    {
        // send measure command
        tempus_buf[0] = CMD_MEASURE;
        tempus_buf[1] = 0;
        tempus_buf[2] = 0;
        tempus_write(tempus_buf, 3);
		osDelay(3);
        // waiting till measurement over
        check_busy();
		//osDelay(3);
        // read measured data
        tempus_read(tempus_buf, 7);
		//osDelay(3);
        // waiting till measurement over
        //check_busy();

        sum_sensor += (tempus_buf[1]<<16 | tempus_buf[2]<<8 | tempus_buf[3]);
        sum_temp   += (tempus_buf[4]<<16 | tempus_buf[5]<<8 | tempus_buf[6]);
    }

    g_Tempus.sensor = (float)(sum_sensor/TEMPUS_AVR_COUNT);
    g_Tempus.temperature = (float)(sum_temp/TEMPUS_AVR_COUNT);

    g_Tempus.sensor = ((g_Tempus.sensor/16777216.0)-0.5)*1000.0;
    g_Tempus.temperature = ((g_Tempus.temperature/16777216.0)*125.0 - 40.0);

    g_Tempus.t_obj = g_Tempus.sensor + g_Tempus.temperature;



    return 0;
}
*/
/*
int8_t bsp_tempus_test(void)
{
	// send measure command
    tempus_buf[0] = CMD_MEASURE;
    tempus_buf[1] = 0;
    tempus_buf[2] = 0;
    tempus_write(tempus_buf, 3);
	osDelay(10);
    // waiting till measurement over
    check_busy();

	return 0;
}
*/
/*
int8_t bsp_tempus_i2c_read()
{
    char count = 0;
    uint32_t sum_sensor, sum_temp;
    uint8_t busy, i;

    sum_sensor = sum_temp = 0;

	bsp_i2c_init( I2C_ADDR_TEMPUS);

    for(count = 0; count < TEMPUS_AVR_COUNT; count++)
    {
        // send measure command
        tempus_buf[0] = CMD_MEASURE;
        tempus_buf[1] = 0;
        tempus_buf[2] = 0;
        bsp_i2c_write( tempus_buf, 3);
		osDelay(3);
        // waiting till measurement over
        i = 0;
        while(1)
	    {
			bsp_i2c_read( &busy, 1);

	        if ((busy&0x20) ==0)
	        {
	            break;
	        }
	        osDelay(10);

	        i++;
	        if (i > 10)
	        {
	           // LOG("Tempus busy check error!!!\n");
	            break;
	        }
	    }

        // read measured data
        bsp_i2c_read( tempus_buf, 7);

        sum_sensor += (tempus_buf[1]<<16 | tempus_buf[2]<<8 | tempus_buf[3]);
        sum_temp   += (tempus_buf[4]<<16 | tempus_buf[5]<<8 | tempus_buf[6]);
    }

    g_Tempus.sensor = (float)(sum_sensor/TEMPUS_AVR_COUNT);
    g_Tempus.temperature = (float)(sum_temp/TEMPUS_AVR_COUNT);

    g_Tempus.sensor = ((g_Tempus.sensor/16777216.0)-0.5)*1000.0;
    g_Tempus.temperature = ((g_Tempus.temperature/16777216.0)*125.0 - 40.0);

    g_Tempus.t_obj = g_Tempus.sensor + g_Tempus.temperature;



    return 0;
}
*/
/*
void bsp_write_mem_register(uint8_t addr,uint8_t dat1, uint8_t dat2)
{
	uint8_t mem_addr = addr + 0x40;

	bsp_sw_i2c_StartCondition();
	bsp_sw_i2c_WriteByte(TEMPUS_I2C_ADR_W);
	bsp_sw_i2c_WriteByte(mem_addr);
	bsp_sw_i2c_WriteByte(dat1);
	bsp_sw_i2c_WriteByte(dat2);
	bsp_sw_i2c_StopCondition ();
	check_busy();

}

void bsp_read_mem_register(uint8_t addr)
{
	bsp_sw_i2c_StartCondition();
	bsp_sw_i2c_WriteByte(TEMPUS_I2C_ADR_W);
	bsp_sw_i2c_WriteByte(addr);
	bsp_sw_i2c_WriteByte(0x80);
	bsp_sw_i2c_WriteByte(0xa9);
	bsp_sw_i2c_StopCondition();

	check_busy();

	bsp_sw_i2c_StartCondition();
	bsp_sw_i2c_WriteByte(TEMPUS_I2C_ADR_R);
	g_Tempus.status = bsp_sw_i2c_ReadByte(0);
	g_Tempus.mem1 = bsp_sw_i2c_ReadByte(0);
	g_Tempus.mem2 = bsp_sw_i2c_ReadByte(1);
	bsp_sw_i2c_StopCondition ();


}

void bsp_mode_write(uint8_t cmd)
{
	bsp_sw_i2c_StartCondition();
	bsp_sw_i2c_WriteByte(TEMPUS_I2C_ADR_W);
	bsp_sw_i2c_WriteByte(cmd);
	bsp_sw_i2c_StopCondition();

	check_busy();
}
*/
